<?php

if (strpos($_SERVER['REQUEST_URI'], '/bottino') === 0) {
	$_SERVER['REQUEST_URI'] = str_replace("/bottino", '', $_SERVER['REQUEST_URI']);
	require("bottino/index.php");
} elseif (strpos($_SERVER['REQUEST_URI'], '/noautopin') === 0) {
	echo "Nothing for now :)";
} else {
	echo "Uh Hello! This is only an index. <br />Go away!";
}