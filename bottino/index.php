<?php
	if (in_array($_SERVER['REQUEST_URI'], ['/', '/index.php', '/index.html', '/index'])) {
		header("Location: https://docs.masterpoll.xyz/");
		die;
	}
	header("Content-Type: application/json; charset=utf-8");
	ini_set('error_log', "/var/log/masterpoll/int-bottino.log");
	$integ = true;
	$apifile = "/home/masterpoll-documents/integrations/int-bottino.php";
	if (file_exists($apifile)) {
		if (!@require($apifile)) {
			if (substr(php_sapi_name(), 0, 3) == 'cgi') {
				header("Status: 502 Bad Gateway");
			} else {
				header("HTTP/1.1 502 Bad Gateway");
			}
			echo json_encode(['ok' => false, 'error_code' => 502, 'description' => "Bad Gateway"]);
			die;
		}
	} else {
		if (substr(php_sapi_name(), 0, 3) == 'cgi') {
			header("Status: 502 Bad Gateway");
		} else {
			header("HTTP/1.1 502 Bad Gateway");
		}
		echo json_encode(['ok' => false, 'error_code' => 502, 'description' => "Bad Gateway"]);
		die;
	}
?>